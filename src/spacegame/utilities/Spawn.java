package spacegame.utilities;

import java.util.Random;

import spacegame.creatures.Alien;
import spacegame.creatures.BossAlien;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.items.Fireball;
import spacegame.items.Bonus;

public class Spawn {
	
	private Handler handler;
	private Health health;
	private Random random = new Random();
	
	public Spawn(Handler handler, Health health) {
		this.handler = handler;
		this.health = health;
	}
	
	public void tick() {
		
		if(health.getScore()%1000 == 0) {
			health.setLevel(health.getLevel() + 1);	
		}
		
		if(health.getLevel() == 1) {
			int spawn = random.nextInt(30);
			if(spawn == 0) handler.addObject(new Alien(random.nextInt(Clamp.WIDTH), 0, ID.EasyAlien, handler));
			
			int spawn2 = random.nextInt(250);
			if(spawn2 == 0) handler.addObject(new Fireball(random.nextInt(Clamp.WIDTH), 0, ID.Fireball, handler));
		} else if(health.getLevel() == 2) {
			int spawn = random.nextInt(20);
			if(spawn == 0) handler.addObject(new Alien(random.nextInt(Clamp.WIDTH), 0, ID.MediumAlien, handler));
			
			int spawn2 = random.nextInt(200);
			if(spawn2 == 0) handler.addObject(new Fireball(random.nextInt(Clamp.WIDTH), 0, ID.Fireball, handler));
		} else if(health.getLevel() == 3) {
			int spawn = random.nextInt(15);
			if(spawn == 0) handler.addObject(new Alien(random.nextInt(Clamp.WIDTH), 0, ID.HardAlien, handler));
			
			int spawn2 = random.nextInt(150);
			if(spawn2 == 0) handler.addObject(new Fireball(random.nextInt(Clamp.WIDTH), 0, ID.Fireball, handler));
		} else if(health.getLevel() == 4) {
			if(health.getScore()%1000 == 0) {
				handler.clearEnemies();
				handler.addObject(new BossAlien((Clamp.WIDTH / 2) - (BossAlien.SIZEX / 2), -BossAlien.SIZEY - 2, ID.BossAlien, handler));
			}
		} else if(health.getLevel() == 5) {
			handler.removeAll();
		}
		int spawn3 = random.nextInt(250);
		if(spawn3 == 0) handler.addObject(new Bonus(random.nextInt(Clamp.WIDTH), 0, ID.Bonus, handler));
	}
}
