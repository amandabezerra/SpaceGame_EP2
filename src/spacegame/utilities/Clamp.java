package spacegame.utilities;

public class Clamp {
	public static final int WIDTH = 500;
	public static final int HEIGHT = 500;
	
	public static float clamp(float var, float min, float max) {
		if(var >= max) {
			return var = max;
		} else if(var <= min) {
			return var = min;
		}
		return var;
	}
}

