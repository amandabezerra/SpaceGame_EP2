package spacegame.utilities;

import java.awt.Color;
import java.awt.Graphics;

public class Health {
	
	public static float initHealth = 100;
	private float health = initHealth;
	private float greenValue = 255;
	
	private int score;
	private int level;
	
	public void tick() {
		health = Clamp.clamp(health, 0, 100);
		greenValue = Clamp.clamp(greenValue, 0, 255);
		greenValue = health * 2;
		
		score++;
	}
	
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(14, 14, 202, 22);
		g.setColor(Color.gray);
		g.fillRect(15, 15, 200, 20); 
		g.setColor(new Color(75, (int) greenValue, 0));
		g.fillRect(15, 15, (int) (health * 2), 20);
		
		g.setColor(Color.white);
		g.drawString("Score: " + score, 14, 60);
		g.drawString("Level: " + level, 14, 76);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public float getHealth() {
		return health;
	}

	public void setHealth(float health) {
		this.health = health;
	}
}
