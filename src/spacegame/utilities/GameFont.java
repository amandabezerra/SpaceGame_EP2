package spacegame.utilities;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

public class GameFont {
	private Font smallFont;
	private Font normalFont;
	public GameFont() {
		try {
			normalFont = Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/edgeracer.ttf")).deriveFont(Font.PLAIN, 80);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
		try {
			smallFont = Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/edgeracer.ttf")).deriveFont(Font.PLAIN, 30);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}
	public Font getSmallFont() {
		return smallFont;
	}
	
	public Font getNormalFont() {
		return normalFont;
	}	
}
