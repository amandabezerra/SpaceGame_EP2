package spacegame.input;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import spacegame.Game;
import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.items.Laser;
import spacegame.sounds.AudioPlayer;

public class KeyInput extends KeyAdapter {
	
	private Handler handler;
	private boolean[] keyDown = new boolean[4];

	public KeyInput(Handler handler, Game game) {
		this.handler = handler;
		keyDown[0] = false;
		keyDown[1] = false;
		keyDown[2] = false;
		keyDown[3] = false;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Player) {
				if(key == KeyEvent.VK_UP) {
					tempObject.setSpeedY(-5);
					keyDown[0] = true;
				}
				if(key == KeyEvent.VK_DOWN) {
					tempObject.setSpeedY(5);
					keyDown[1] = true;
				}
				if(key == KeyEvent.VK_LEFT) {
					tempObject.setSpeedX(-5);
					keyDown[2] = true;
				}
				if(key == KeyEvent.VK_RIGHT) {
					tempObject.setSpeedX(5);
					keyDown[3] = true;
				}
				
				if(key == KeyEvent.VK_SPACE) {
					handler.addObject(new Laser(tempObject.getX() + Laser.SIZEX, tempObject.getY() - (Laser.SIZEY / 2) - 6, ID.Laser, handler));
					
					AudioPlayer.getSound("laser").play();
				}
			}
		}
		
		if(key == KeyEvent.VK_P) {
			if(Game.paused) {
				Game.paused = false;
			} else {
				Game.paused = true;
			}
		}
		
		if(key == KeyEvent.VK_ESCAPE) {
			System.exit(1);
		}
		
		if(key == KeyEvent.VK_M) {
			Game.stop();
		}
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Player) {
			
				if(key == KeyEvent.VK_UP) {
					keyDown[0] = false;
				}
				if(key == KeyEvent.VK_DOWN) {
					keyDown[1] = false;
				}
				if(key == KeyEvent.VK_LEFT) {
					keyDown[2] = false;
				}
				if(key == KeyEvent.VK_RIGHT) {
					keyDown[3] = false;
				}
				if(!keyDown[0] && !keyDown[1]) {
					tempObject.setSpeedY(0);
				}
				if(!keyDown[2] && !keyDown[3]) {
					tempObject.setSpeedX(0);
				}
			}
		}
	}
}
