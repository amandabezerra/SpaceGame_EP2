package spacegame.items;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;

public class BossAlienAttack extends GameObject {
	
	private Handler handler;
	
	public static int SIZEX = 40;
	public static int SIZEY = 40;
	
	Random random = new Random();	
	
	private BufferedImage bossAttackImage;

	public BossAlienAttack(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		speedX = (random.nextInt(5 - -5) + -5);
		speedY = 2;
		
		ImageLoader attack = new ImageLoader("/images/bossattack.png");
		bossAttackImage = attack.getImage(1, 1, SIZEX, SIZEY);
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		if(y <= 0 || y >= Clamp.HEIGHT - (SIZEY/2)) {
			speedY *= -1;
		}
		if(x <= 0 || x >= Clamp.WIDTH - (SIZEX/2)) {
			speedX *= -1;
		}
		
		collision();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bossAttackImage, (int) x, (int) y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
	
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Laser) {
				if(getBounds().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}
			}
		}
	}
}
