package spacegame.items;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;

public class Bonus extends GameObject {

	private int SIZEX = 32;
	private int SIZEY = 32;
	
	private Handler handler;
	private BufferedImage bonusImage;
	
	public Bonus(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		
		this.handler = handler;
		 
		ImageLoader loader = new ImageLoader("/images/bonus.png");
		bonusImage = loader.getImage(1, 1, SIZEX, SIZEY);
		
		speedY = 2;
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		if(y >= Clamp.HEIGHT) handler.removeObject(this);
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bonusImage, (int)x, (int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
}
