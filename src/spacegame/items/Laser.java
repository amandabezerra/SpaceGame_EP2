package spacegame.items;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;

public class Laser extends GameObject {
	
	private Handler handler;
	
	public static int SIZEX = 6;
	public static int SIZEY = 18;
	
	Random random = new Random();
	
	private BufferedImage laserImage;

	public Laser(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		ImageLoader loader = new ImageLoader("/images/laser.png");
		laserImage = loader.getImage(1, 1, SIZEX, SIZEY);		
		
		speedY = -2;
	}

	@Override
	public void tick() {
		y += speedY;
		
		if(y >= Clamp.HEIGHT) handler.removeObject(this);	
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(laserImage, (int)x, (int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
}
