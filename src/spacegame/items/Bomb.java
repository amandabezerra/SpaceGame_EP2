package spacegame.items;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import spacegame.creatures.Player;
import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;

public class Bomb extends GameObject {
	
	private Handler handler;
	private GameObject player;
	
	private int SIZEX = 64;
	private int SIZEY = 64;
	
	private BufferedImage bombImage;

	public Bomb(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		for(int i = 0; i < handler.object.size(); i++ ){
            if(handler.object.get(i).getId() == ID.Player){
            	player = handler.object.get(i);
            }
		}
		 
		ImageLoader loader = new ImageLoader("/images/bomb.png");
		bombImage = loader.getImage(1, 1, SIZEX, SIZEY);		
		 
		speedY = 2;
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		
		float diferenceX = x - player.getX() + (Player.SIZEX / 2);
		float distance = (float) Math.hypot(x - player.getX(), y - player.getY());
		
		speedX = ((-1 / distance) * diferenceX);
		
		if(y >= Clamp.HEIGHT) handler.removeObject(this);
		
		collision();
	}

	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Laser) {
				if(getBounds().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}
			}
		}
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bombImage, (int)x, (int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
}