package spacegame;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import spacegame.utilities.GameFont;

public class ComponentDesigner {
	
	private GameFont fonts = new GameFont();
	
	public JPanel createCard() {
		JPanel card = new JPanel();
		card.setBounds(0, 0, 500, 500);
		card.setLayout(null);
		
		return card;
	}
	
	public JButton createButton(String btnName, int x, int y, int width, int height) {
		JButton btn = new JButton(btnName);
		btn.setBounds(x, y, width, height);
		btn.setFont(fonts.getSmallFont());
		btn.setForeground(Color.YELLOW);
		btn.setBackground(Color.DARK_GRAY);
		
		return btn;
	}
	
	public JLabel createLabel(String lblMessage, int x, int y, int width, int height, int type) {
		JLabel lbl = new JLabel(lblMessage);
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		if(type == 1) {
			lbl.setFont(fonts.getSmallFont());
		} else if(type == 2) {
			lbl.setFont(fonts.getNormalFont());
		}
		lbl.setForeground(Color.YELLOW);
		lbl.setBackground(Color.DARK_GRAY);
		lbl.setBounds(x, y, width, height);
		
		return lbl;
	}
	
	public JTextPane createTextPane(String textMessage, int x, int y, int width, int height, int type) {
		StyleContext context = new StyleContext();
		Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
	    StyleConstants.setAlignment(style, StyleConstants.ALIGN_CENTER);
		style.addAttribute(StyleConstants.Foreground, Color.YELLOW);
		StyledDocument document = new DefaultStyledDocument(context);
		try {
			document.insertString(document.getLength(), textMessage, context.getStyle(StyleContext.DEFAULT_STYLE));
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		JTextPane textPane = new JTextPane(document);
	    textPane.setEditable(false);
	    textPane.setOpaque(false);
	    textPane.setBounds(x, y, width, height);
	    if(type == 1) {
	    	textPane.setFont(fonts.getSmallFont());
	    } else if(type == 2) {
	    	textPane.setFont(fonts.getNormalFont());
	    }
	    
	    return textPane;
	}
	
	public JTextPane printRanking() {
		StyleContext context = new StyleContext();
		Style style = context.getStyle(StyleContext.DEFAULT_STYLE);
	    StyleConstants.setAlignment(style, StyleConstants.ALIGN_LEFT);
	    style.addAttribute(StyleConstants.Foreground, Color.YELLOW);
		StyledDocument document = new DefaultStyledDocument(context);
		
		Ranking ranking = new Ranking();
		ArrayList <String> rankingList = new ArrayList<String>();
		rankingList = ranking.readRankingFromFile();
		Collections.sort(rankingList, Collections.reverseOrder());
		
		try {
			for(int i = 0; i <= 7; i++) {
				if(i < rankingList.size()) {
					document.insertString(document.getLength(), rankingList.get(i) + "\n", context.getStyle(StyleContext.DEFAULT_STYLE));
				}		
			}
			
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		JTextPane textPane = new JTextPane(document);
	    textPane.setEditable(false);
	    textPane.setOpaque(false);
	    textPane.setBounds(50, 90, 450, 300);
	    textPane.setFont(fonts.getSmallFont());
	    
	    return textPane;
	}
	
	public JLabel createBackgroundImage() {
		JLabel background = new JLabel(new ImageIcon("res/images/space.jpg"));
		background.setBounds(0, 0, 500, 500);
		
		return background;
	}
	
	public JLabel createIcon(String path, int x, int y, int width, int height) {
		JLabel background = new JLabel(new ImageIcon(path));
		background.setBounds(x, y, width, height);
		
		return background;
	}
	
	public JTextField createTextField() {
		JTextField textFieldName = new JTextField();
		textFieldName.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldName.setBounds(150, 200, 200, 25);
		textFieldName.setForeground(Color.YELLOW);
		textFieldName.setBackground(Color.DARK_GRAY);
		textFieldName.setColumns(70);
		
		return textFieldName;
	}
	
	
}
