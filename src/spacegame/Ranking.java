package spacegame;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Ranking implements Comparable<Ranking>{
	
	private String name;
	private int score;
	
	public Ranking() {
		
	}
	
	public Ranking(String name, int score) {
		this.name = name;
		this.score = score;
		
		toString();
	}
	
	public String toString() {
		String s = this.getName() + String.format("%5d", this.getScore());	
		return s;
	}
	
	@Override
	public int compareTo(Ranking o) {
		if(this.score < o.score) {
			return -1;
		}
		if(this.score > o.score) {
			return 1;
		}
		return 0;
	}
	
	private void writeToFile(String info) throws IOException {
		File f = new File("res/docs/ranking.txt");
		FileWriter fw = new FileWriter(f, true);
		try {
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(info);
			bw.flush();
			bw.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> readRankingFromFile() {
		ArrayList <String> ranking = new ArrayList<String>();
		
		File f = new File("res/docs/ranking.txt");
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				ranking.add(br.readLine());
			}
			br.close();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		return ranking;
	}
	
	public void addToRanking() {
		try {
			writeToFile(this.toString());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void printRanking(ArrayList <String> ranking) {
		Collections.sort(ranking, Collections.reverseOrder());
		for(int i = 0; i < ranking.size(); i++) {
			System.out.println(ranking.get(i));
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
