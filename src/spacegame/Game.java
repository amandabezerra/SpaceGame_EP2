package spacegame;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JPanel;

import spacegame.creatures.Player;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.input.KeyInput;
import spacegame.utilities.GameFont;
import spacegame.utilities.Health;
import spacegame.utilities.Spawn;

public class Game extends JPanel implements Runnable {
	private static final long serialVersionUID = -8354725971538312971L;
	public static final int WIDTH = 500;
	public static final int HEIGHT = 500;
	
	private static boolean running = false;
	public static boolean paused = false;
	
	private static Thread thread;
	private Handler handler;
	private Health health;
	private Spawn spawner;
	private GameFont fonts = new GameFont();
	public static enum STATE {
		Game,
		End
	};
	
	public static STATE gameState = STATE.Game;
	JButton btnBack;
	
	public Game(JButton btnBack) {
		handler = new Handler();
		setLayout(null);
		this.btnBack = btnBack;
		
		this.addKeyListener(new KeyInput(handler, this));
		
		health = new Health();
		health.setScore(0);
		health.setLevel(1);
		spawner = new Spawn(handler, health);
		new Random();
		
		handler.addObject(new Player(WIDTH/2 - Player.SIZEX, HEIGHT + Player.SIZEY, ID.Player, handler, health));
	}

	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized static void stop() {
		try {
			thread.join();
			running = false;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		this.requestFocus();
		
		int fps = 60;
		double timePerTick = 1000000000 / fps;
		double delta = 0;
		long now;
		long lastTime = System.nanoTime();
		
		while(running) {
			now = System.nanoTime();
			delta += (now - lastTime) / timePerTick;
			lastTime = now;
			
			if(delta >= 1) {
				tick();
				repaint();
				delta--;
			}
		}
		stop();
	}
	
	private void tick() {		
		if(gameState == STATE.Game) {
			if(!paused) {
				handler.tick();
				health.tick();
				spawner.tick();
				
				if(health.getHealth() <= 0 || health.getLevel() == 5) {
					health.setHealth(Health.initHealth);

					gameState = STATE.End;
					handler.clearEnemies();
				}
			}
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setPaintMode();
		
		if(paused) {
			g.setColor(Color.YELLOW);
			g.drawString("PAUSED", 230, 100);
		}
		
		if(gameState == STATE.Game) {
			health.render(g);
			handler.render(g);
		} else if(gameState == STATE.End) {
			btnBack.setVisible(true);
			handler.clearEnemies();
			g.setFont(fonts.getNormalFont());
			g.setColor(Color.YELLOW);
			if(health.getLevel() == 5) {
				g.drawString(" you", 138, 200);
				g.drawString(" win", 145, 250);
			} else {
				g.drawString("game", 138, 200);
				g.drawString("over", 145, 250);
			}
			g.setFont(fonts.getSmallFont());
			g.drawString("your score : " + health.getScore(), 120, 300);
		}
	
		g.dispose();
	}
	
	public int getScore() {
		return health.getScore();
	}
}

