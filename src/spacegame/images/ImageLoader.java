package spacegame.images;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {
	
	BufferedImage image;
	private String path;
	
	public ImageLoader(String path) {
		this.path = path;
		loadImage();
	}
	
	public BufferedImage loadImage() {
		try {
			image = ImageIO.read(getClass().getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	}
	
	public BufferedImage getImage(int column, int row, int width, int height) {
		BufferedImage img = image.getSubimage((row * 32) - 32, (column * 32) - 32,  width, height);
		return img;
	}
}
