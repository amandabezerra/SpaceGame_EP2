package spacegame;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import spacegame.sounds.AudioPlayer;

public class Menu extends JFrame implements Runnable {

	private static final long serialVersionUID = -8242077720940486798L;
	private JPanel contentPane;
	private ComponentDesigner designer;
	private CardLayout cardManage;
	private Game cardGame;
	private JTextField txtRegistration;
	private String namePlayer;
	private int scorePlayer;

	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
		setTitle("SPACE COMBAT");
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout());
		contentPane.setBounds(0, 0, 500, 500);
		
		designer = new ComponentDesigner();
		cardManage = new CardLayout();
		cardManage = (CardLayout) contentPane.getLayout();
		
		initializeCardMain();
		initializeCardInstruction();
		initializeCardRegistration();
		initializeCardRanking();
		initializeCardGame();
		
		AudioPlayer.load();
		AudioPlayer.getMusic("background").loop();
	}

	private void initializeCardGame() {
		
		JButton btnBack = designer.createButton("back to menu", 0, 470, 500, 30);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Main");
				scorePlayer = cardGame.getScore();
				AudioPlayer.getSound("click").play();
				if(!namePlayer.isEmpty()) {
					Ranking cardGameRanking = new Ranking(namePlayer, scorePlayer);
					cardGameRanking.addToRanking();
				}
			}
		});
		
		btnBack.setVisible(false);
		cardGame = new Game(btnBack);
		contentPane.add(cardGame, "Game");
		cardGame.add(btnBack);
		
		cardGame.add(designer.createBackgroundImage());
	}

	private void initializeCardMain() {
		JPanel cardMain = designer.createCard();
		
		JTextPane textTitle = designer.createTextPane("space\ngame", 25, 20, 450, 150, 2);
		cardMain.add(textTitle);
		
		JButton btnPlay = designer.createButton("play", 125, 200, 250, 40);
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Registration");
				AudioPlayer.getSound("click").play();
			}
		});
		cardMain.add(btnPlay);
		
		JButton btnRanking = designer.createButton("ranking", 125, 260, 250, 40);
		btnRanking.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Ranking");
				AudioPlayer.getSound("click").play();
			}
		});
		cardMain.add(btnRanking);
		
		JButton btnNext = designer.createButton("instructions", 125, 320, 250, 40);
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Instructions");
				AudioPlayer.getSound("click").play();
			}
		});
		cardMain.add(btnNext);
		
		JButton btnQuit = designer.createButton("quit", 125, 380, 250, 40); 
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		cardMain.add(btnQuit);
		
		cardMain.add(designer.createBackgroundImage());
		
		contentPane.add(cardMain, "Main");
	}
	
	
	private void initializeCardInstruction() {
		JPanel cardInstruction = designer.createCard();
		
		JButton btnBack = designer.createButton("back", 125, 400, 250, 40);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Main");
				AudioPlayer.getSound("click").play();
			}
		});
		cardInstruction.add(btnBack);
		
		JTextPane textInstruction = designer.createTextPane("to go up\n\n"
				+ "to go down\n\n"
				+ "to move left\n\n"
				+ "to move right\n\n"
				+ "p to pause or unpause\n\n"
				+ "spacebar to shoot", 50, 30, 400, 400, 1);
		cardInstruction.add(textInstruction);
		
		cardInstruction.add(designer.createIcon("res/images/up.png", 60, 30, 44, 44));
		cardInstruction.add(designer.createIcon("res/images/down.png", 60, 90, 44, 44));
		cardInstruction.add(designer.createIcon("res/images/left.png", 60, 150, 44, 44));
		cardInstruction.add(designer.createIcon("res/images/right.png", 60, 210, 44, 44));
		
		cardInstruction.add(designer.createBackgroundImage());
		
		contentPane.add(cardInstruction, "Instructions");
	}
	
	private void initializeCardRegistration() {
		JPanel cardRegistration = designer.createCard();
		
		cardRegistration.add(designer.createLabel("enter your name", 100, 100, 300, 40, 1));
		
		txtRegistration = designer.createTextField();
		cardRegistration.add(txtRegistration);
		
		JButton btnStart = designer.createButton("start", 125, 300, 250, 40);
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Game");
				cardGame.start();
				namePlayer = txtRegistration.getText();
				AudioPlayer.getSound("click").play();
			}
		});
		cardRegistration.add(btnStart);
		
		JButton btnBack = designer.createButton("back", 125, 400, 250, 40);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Main");
				AudioPlayer.getSound("click").play();
			}
		});
		cardRegistration.add(btnBack);

		cardRegistration.add(designer.createBackgroundImage());
		
		contentPane.add(cardRegistration, "Registration");
	}
	
	private void initializeCardRanking() {
		JPanel cardRanking = designer.createCard();
		
		cardRanking.add(designer.createLabel("ranking", 100, 50, 300, 40, 1));
		
		JButton btnBack = designer.createButton("back", 125, 400, 250, 40);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cardManage.show(contentPane, "Main");
				AudioPlayer.getSound("click").play();
			}
		});
		cardRanking.add(btnBack);
		
		JTextPane txtRanking = designer.printRanking();
		cardRanking.add(txtRanking);
		
		cardRanking.add(designer.createBackgroundImage());
		
		contentPane.add(cardRanking, "Ranking");
	}

	@Override
	public void run() {
		this.setVisible(true);
	}

	public String getNamePlayer() {
		return namePlayer;
	}

	public void setNamePlayer(String namePlayer) {
		this.namePlayer = namePlayer;
	}
}
