package spacegame.creatures;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.items.BossAlienAttack;
import spacegame.utilities.Clamp;

public class BossAlien extends GameObject {
	
	private Handler handler;
	
	private int timer = 80;
	private int timer2 = 50;
	
	Random random = new Random();
	
	private BufferedImage bossImage;

	public BossAlien(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		SIZEX = 132;
		SIZEY = 132;
		
		speedX = 0;
		speedY = 2;
		
		ImageLoader boss = new ImageLoader("/images/boss.png");
		bossImage = boss.getImage(1, 1, SIZEX, SIZEY);
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		if(timer <= 0) speedY = 0;
		else timer--;
		
		if(timer <= 0) timer2--;
		if(timer2 <= 0) {
			if(speedX == 0) speedX = 2;
			
			if(speedX > 0) speedX += 0.005f; //get slowly gradually
			else if(speedX < 0) speedX -= 0.005f; //get fast again
			
			speedX = Clamp.clamp(speedX, -15, 15);
			
			int spawn = random.nextInt(20);
			if(spawn == 0) {
				handler.addObject(new BossAlienAttack((int)x + SIZEX, (int)y + SIZEY - 44, ID.BossAlienAttack, handler));
			}
		}
		if(x <= 0 || x >= Clamp.WIDTH - SIZEX) speedX *= -1;	
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(bossImage, (int)x,(int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
}
