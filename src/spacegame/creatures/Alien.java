package spacegame.creatures;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;

public class Alien extends GameObject {
	
	private Handler handler;
	
	Random random = new Random();
	
	private BufferedImage alienImage;
	private String path;
	
	public Alien(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		SIZEX = 20;
		SIZEY = 20;
		
		if(id == ID.EasyAlien) {
			path = "/images/alien_EASY.png";
		} else if(id == ID.MediumAlien) {
			path = "/images/alien_MEDIUM.png";
		} else if(id == ID.HardAlien) {
			path = "/images/alien_HARD.png";
		}
				
		ImageLoader alien = new ImageLoader(path);
		alienImage = alien.getImage(1, 1, SIZEX, SIZEY);	
				
		speedY = 2;
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		if(y >= Clamp.HEIGHT) handler.removeObject(this);
		
		collision();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(alienImage, (int) x, (int) y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
	
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Laser) {
				if(getBounds().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}
			}
		}
	}
}
