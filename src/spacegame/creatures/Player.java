package spacegame.creatures;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.images.ImageLoader;
import spacegame.utilities.Clamp;
import spacegame.utilities.Health;

public class Player extends GameObject {

	private Handler handler;
	private Health health;
	public static int SIZEX = 18;
	public static int SIZEY = 41;
	
	private BufferedImage playerImage;
		
	public Player(int x, int y, ID id, Handler handler, Health health) {
		super(x, y, id);
		this.handler = handler;
		this.health = health;
		
		ImageLoader loader = new ImageLoader("/images/spaceship.png");
		playerImage = loader.getImage(1, 1, SIZEX, SIZEY);
	}
	
	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		x = Clamp.clamp(x, 0, Clamp.WIDTH - SIZEX);
		y = Clamp.clamp(y, 0, Clamp.HEIGHT - SIZEY);
		
		collision();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(playerImage, (int) x, (int) y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
	
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.EasyAlien 
					|| tempObject.getId() == ID.MediumAlien
					|| tempObject.getId() == ID.HardAlien
					|| tempObject.getId() == ID.Fireball
					|| tempObject.getId() == ID.BossAlienAttack) {
				if(getBounds().intersects(tempObject.getBounds())) {
					health.setHealth(health.getHealth() - 10);
					handler.removeObject(tempObject);
				}
			} else if(tempObject.getId() == ID.Bonus) {
				if(getBounds().intersects(tempObject.getBounds())) {
					health.setHealth(health.getHealth() + 10);
					handler.removeObject(tempObject);
				}
			}
		}
	}
}
