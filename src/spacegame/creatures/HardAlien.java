package spacegame.creatures;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import spacegame.definitions.GameObject;
import spacegame.definitions.Handler;
import spacegame.definitions.ID;
import spacegame.utilities.Clamp;

public class HardAlien extends GameObject {
	
	private Handler handler;
	
	public static int SIZEX = 16;
	public static int SIZEY = 16;
	
	Random random = new Random();	

	public HardAlien(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		speedY = 2;
	}

	@Override
	public void tick() {
		x += speedX;
		y += speedY;
		
		if(y >= Clamp.HEIGHT) handler.removeObject(this);
		
		collision();
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillRect((int) x, (int) y, SIZEX, SIZEY);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, SIZEX, SIZEY);
	}
	
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.Laser) {
				if(getBounds().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}
			}
		}
	}

}
