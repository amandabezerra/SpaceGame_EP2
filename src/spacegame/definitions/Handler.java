package spacegame.definitions;

import java.awt.Graphics;
import java.util.ArrayList;

public class Handler {
	
	public ArrayList <GameObject> object = new ArrayList<GameObject>();
	
	public void tick() {
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			
			tempObject.tick();
		}
	}
	
	public void render(Graphics g) {
		try{
			for(int i = 0; i < object.size(); i++) {
				GameObject tempObject = object.get(i);
				tempObject.render(g);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addObject(GameObject object) {
		this.object.add(object);
	}
	
	public void removeObject(GameObject object) {
		this.object.remove(object);
	}
	
	public void clearEnemies() {		
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			if(tempObject.getId() != ID.Player) {
				removeObject(tempObject);
				i--;
			}
		}
	}

	public void removeAll() {
		for(int i = 0; i < object.size(); i++) {
			GameObject tempObject = object.get(i);
			removeObject(tempObject);
		}
	}
}
