package spacegame.definitions;

public enum ID {
	Player(),
	Laser(),
	EasyAlien(),
	MediumAlien(),
	HardAlien(),
	BossAlien(),
	BossAlienAttack,
	Fireball(),
	Bonus();
}
